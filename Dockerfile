FROM fedora:32
RUN dnf -y install procs util-linux-user nss-pam-ldapd passwd pam_mount nfs-utils procps vim
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN  chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD [ "/opt/docker/startup.sh" ]

